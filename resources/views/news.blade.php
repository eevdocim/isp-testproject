@extends('layouts.app')
<style>
    a{text-decoration: none!important; color: black!important;}
    form{display: flex; flex-direction: column;}
    form .form-control{margin: 5px;}
    .card-header{display: flex; flex-direction: column;}
    .news{padding: 10px;}
    .news-body{display: flex; flex-direction:row;}
    .news-body .left{display: flex; flex-direction: column; justify-content: space-between; width: 100%;}
    .news-body .left .date{border-top: 1px solid #d0d0d0; width: 100%;}
    .news-body .icon img{width: 250px; margin-right: 20px;}
</style>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ __('Новость') }}
                    @if(auth()->user()->role == 1)
                        <news-button-delete-component id="{{$news->id}}"></news-button-delete-component>
                        <news-button-status-component id="{{$news->id}}" status="{{$news->status}}"></news-button-status-component>
                    @endif
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <news-component href = "/" title="{{$news->name}}" img="{{($news->img ? $news->img :'/storage/empty.png')}}" text="{{$news->text}}" date="{{$news->created_at}}"></news-component>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
