@extends('layouts.app')
<style>
    a{text-decoration: none!important; color: black!important;}
    form{display: flex; flex-direction: column;}
    form .form-control{margin: 5px;}
    .card-header{display: flex; flex-direction: column;}
    .news{padding: 10px;}
    .news-body{display: flex; flex-direction:row;}
    .news-body .left{display: flex; flex-direction: column; justify-content: space-between; width: 100%;}
    .news-body .left .date{border-top: 1px solid #d0d0d0; width: 100%;}
    .news-body .icon img{width: 250px; margin-right: 20px;}
    .news-body .text {text-overflow: ellipsis; width: 25px; overflow: hidden; white-space: nowrap;}
</style>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ __('Новости') }}
                    @if(auth()->user()->role == 1)
                        <news-button-add-component></news-button-add-component>
                        @endif
                </div>
                <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <news-modal-add-component route="/api/news"></news-modal-add-component>
                        <news-list-component></news-list-component>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
