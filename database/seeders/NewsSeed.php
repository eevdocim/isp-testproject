<?php

namespace Database\Seeders;

use App\Models\News;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class NewsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    static $arr = [
        ["name"=>'name',"text"=>"text","img"=> "https://fakeimg.pl/600x600/", 'created_at' => '04.02.2020 5:38:13','status'=>'0'],
        ["name"=>'name',"text"=>"text","img"=> "https://fakeimg.pl/600x600/", 'created_at' => '16.06.2020 12:38:13','status'=>'1'],
        ["name"=>'name',"text"=>"text","img"=> "https://fakeimg.pl/600x600/", 'created_at' => '08.08.2020 18:38:13','status'=>'1'],
        ["name"=>'name',"text"=>"text","img"=> "https://fakeimg.pl/600x600/", 'created_at' => '14.10.2020 16:38:13','status'=>'1'],
        ["name"=>'name',"text"=>"text","img"=> "https://fakeimg.pl/600x600/", 'created_at' => '07.11.2020 15:38:13','status'=>'1'],
        ["name"=>'name',"text"=>"text","img"=> "https://fakeimg.pl/600x600/", 'created_at' => '24.12.2020 11:38:13','status'=>'1']
    ];
    public function run()
    {
        foreach (self::$arr as $key => $value) {
            News::create($value);
        }
    }
}
