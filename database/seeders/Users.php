<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'test',
            'email' => 'test@test',
            'password' => Hash::make('testtest'),
            'role' => 1,
        ]);

        $user = User::create([
            'name' => 'admin',
            'email' => 'admin@admin',
            'password' => Hash::make('adminadmin'),
            'role' => 1,
        ]);

        $user = User::create([
            'name' => 'user',
            'email' => 'user@user',
            'password' => Hash::make('useruser'),
        ]);
    }
}
