<?php

use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use PHPUnit\TextUI\XmlConfiguration\Group;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => 'auth:sanctum'],function(){
    Route::get('/news', [App\Http\Controllers\NewsController::class, 'index']);
    Route::put('/news', [App\Http\Controllers\NewsController::class, 'create']);
    Route::post('/news', [App\Http\Controllers\NewsController::class, 'updateStatus']);
    Route::delete('/news', [App\Http\Controllers\NewsController::class, 'delete']);
});
