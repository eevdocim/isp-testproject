<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home', ["news" => News::get()]);
    }

    public function news($id=null)
    {
        $news = News::where('id', $id)->first();
        if(auth()->user()->role == 0 && $news->status == 0){
            return redirect()->route('home');
        }

        return view('news',["news"=>$news]);
    }
}
