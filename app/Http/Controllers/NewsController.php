<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request){
        $news = News::orderBy('created_at', 'desc')->get();
        return $news;
    }

    public function create(Request $request){
        if (auth()->user()->role == 1 && $request->input("name") && $request->input("text")) {
            $news = new News();
            $news->name = $request->input("name");
            $news->text = $request->input("text");
            $news->status = 1;
            $news->save();
            if ($request->file("file") && $request->file("file")->isValid()) {
                $destinationPath = Storage::path('public');
                if ($request->file("file")->move($destinationPath, $news->id)) {
                    $news->img = DIRECTORY_SEPARATOR . "storage" . DIRECTORY_SEPARATOR . $news->id;
                    $news->save();
                }
            }
        } else {
            return abort(403);
        }

    }

    public function updateStatus(Request $request){
        if (auth()->user()->role == 1) {
            $news = News::where('id',$request->input("id"))->first();
            $news->status = $request->input("status");
            $news->save();
            return $news;
        } else {
            return abort(403);
        }
    }
    public function delete(Request $request)
    {
        if (auth()->user()->role == 1) {
            $news = News::where('id', $request->input("id"))->first();
            if(!empty($news->img)){
                Storage::disk('public')->delete($news->id);
            }
            News::where('id', $request->input("id"))->delete();
        }else {
            return abort(403);
        }
    }
}
